package com.ideas2it.zomato.cart.service;

import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cartDetail.CartDetail;

/*
 * This interface handles access between Controller and DAO 
 */
public interface CartService {

    /**
     * User adds a new item to the cart.
     *
     * @param menuItemId
     *            - id of the chosen menu item
     *
     * @param userId
     *            - id of cart's user
     */
    void addToCart(int menuItemId, int userId);

    /**
     * Returns the cart for the specified user.
     *
     * @param userId
     *            - id of the user
     *
     * @return cart object for the id
     */
    Cart getCart(int userId);
    
    /**
     * Removes the cart detail specified, from the cart
     *
     * @param cartDetailId
     *            - id of cartDetail
     *
     * @return True or false based on cart detail deletion
     */
    boolean removeCartDetail(int cartDetailId);
}

   
