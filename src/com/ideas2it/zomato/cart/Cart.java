package com.ideas2it.zomato.cart;

import java.util.Set;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.cartDetail.CartDetail;

/**
 * This is model class for Cart 
 */
public class Cart {

    private int id;
    private User user;
    private Set<CartDetail> cartDetails;
    
    public Cart() {
    
    }
    
    public Cart(User user) {
        this.user = user;
    }
    
    /*
     * Setters and Getters
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    } 
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setCartDetails(Set<CartDetail> cartDetails) {
        this.cartDetails = cartDetails;
    }
    
    public Set<CartDetail> getCartDetails() {
        return cartDetails;
    } 
    
    public void addCartDetail(CartDetail cartDetail) {
        cartDetails.add(cartDetail);
    }
}
