package com.ideas2it.zomato.role;

import java.util.Set;

import com.ideas2it.zomato.user.User;

/**
 * Different roles the user could be a part of.
 */
public class Role {

    private int id;
    private String name;
    private Set<User> users;
    
    public Role() {
    
    }
    
    public Role(String name) {
        this.name = name;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setUsers(Set<User> users) {
        this.users = users;
    }
    
    public Set<User> getUsers() {
        return users;
    }
}
