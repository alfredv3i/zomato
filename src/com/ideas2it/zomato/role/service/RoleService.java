package com.ideas2it.zomato.role.service;

import java.util.Set;

import com.ideas2it.zomato.role.Role;
/**
 * Interface implementation for role service class
 */
public interface RoleService {

    /**
     * Adds a new role
     *
     * @param role
     *            - role object about to be added 
     */
    void add(Role role);
    
    /**
     * Deletes the role from database.
     *
     * @param roleId
     *            - id of role to be deleted 
     */
    void delete(int roleId);
    
    /**
     * returns the role specified by id
     *
     * @param id
     *            - id of the role 
     *
     * @return role specified by id
     */
    Role getRole(int id);
    
    /**
     * returns all the user role from database
     *
     * @return set of roles
     */
    Set<Role> getRoles();
}
