package com.ideas2it.zomato.role.service.impl;

import java.util.Set;

import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.role.service.RoleService;
import com.ideas2it.zomato.role.dao.RoleDao;
import com.ideas2it.zomato.role.dao.impl.RoleDaoImpl;

/**
 * Service class implementation for food object
 */
public class RoleServiceImpl implements RoleService {

    RoleDao roleDao = new RoleDaoImpl();
    
    /**
     * Adds a new role to the database.
     *
     * @param role
     *            - role object about to be added 
     */
    public void add(Role role) {
        roleDao.insert(role);
    }
    
    /**
     * Deletes the role from database.
     *
     * @param roleId
     *            - id of role to be deleted 
     */
    public void delete(int roleId) {
        roleDao.delete(roleId);
    }
    
    /**
     * Returns the role specified by id.
     *
     * @param id
     *            - id of the role 
     *
     * @return role specified by id
     */
    public Role getRole(int id) {
        return roleDao.getRole(id);
    }
    
    /**
     * Returns all the user role from database.
     *
     * @return set of roles
     */
    public Set<Role> getRoles() {
        return roleDao.getRoles();
    }
}
