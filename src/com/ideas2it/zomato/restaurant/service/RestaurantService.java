package com.ideas2it.zomato.restaurant.service;

import java.util.Set;

import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.review.Review;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.classification.Classification;

/**
 * Interface implementation for Restaurant service class
 */
public interface RestaurantService {

    /**
     * Registers a new restaurant
     *
     * @param restaurant
     *            - restaurant object about to be registered 
     *
     * @param address
     *            - address of the restaurant
     *
     * @param userId
     *            - id of restaurant's vendor
     *
     * @return true or false based on restaurant registration
     */
    boolean registerRestaurant(Restaurant restaurant, Address address,
                                                          int userId);
    
    /**
     * Adds a new classification
     *
     * @param classification
     *            - classification of the restaurant 
     *
     * @param restaurant
     *            - restaurant object to be updated 
     */
    void assignClassification(Classification classification,
                                        Restaurant restaurant);
    
    /**
     * Returns all the restaurants in the database
     *
     * @param classificationId
     *            - classification Id of restaurants to be fetched
     *
     * @return set of restaurants
     */
    Set<Restaurant> getRestaurants(int classificationId);
    
    /**
     * Adds a new classification
     *
     * @param user
     *            - user object about to be added 
     *
     * @param restaurant
     *            - restaurant object about to be added 
     *
     * @param review
     *            - review object about to be added 
     */
    void addReview(User user, Restaurant restaurant, Review review);
    
    /**
     * Deletes the restaurant
     *
     * @param id
     *            - id of restaurant about to be deleted 
     */
    void deleteRestaurant(int id);
}
