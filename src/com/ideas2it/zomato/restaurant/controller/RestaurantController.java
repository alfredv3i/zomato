package com.ideas2it.zomato.restaurant.controller;

import java.lang.NullPointerException;
import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.classification.service.ClassificationService;
import com.ideas2it.zomato.classification.service.impl.ClassificationServiceImpl;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.review.Review;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.restaurant.service.RestaurantService;
import com.ideas2it.zomato.restaurant.service.impl.RestaurantServiceImpl;

public class RestaurantController {

    RestaurantService restaurantService = new RestaurantServiceImpl();
    ClassificationService classificationService = 
                                                new ClassificationServiceImpl();
    private static final Logger logger = Logger.getLogger(RestaurantController.class); 

    /**
     * Registers a new restaurant
     *
     * @param restaurant
     *            - restaurant object about to be registered 
     *
     * @param address
     *            - address of the restaurant
     *
     * @param userId
     *            - id of restaurant's vendor
     *
     * @return true or false based on restaurant registration
     */
    public boolean registerRestaurant(Restaurant restaurant, Address address,
                                                          int userId) {
        boolean value = false;
        try {
            value = restaurantService.registerRestaurant(restaurant, address,
                                                         userId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return value;
    }
    
    /**
     * Returns all the restaurants in the database
     *
     * @param classificationId
     *            - classification Id of restaurants to be fetched
     *
     * @return set of restaurants
     */
    public Set<Restaurant> getRestaurants(int classificationId) {
        Set<Restaurant> restaurants = new HashSet<>();
        try {
            restaurants = restaurantService.getRestaurants(classificationId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return restaurants;
    }
    
    /**
     * Adds a new classification
     *
     * @param classification
     *            - classification object about to be added 
     */
    public void addClassification(Classification classification) {
        try {
            classificationService.add(classification);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Removes the classification 
     *
     * @param id
     *            - id of classification 
     */
    public void removeClassification(int id) {
        try {
            classificationService.remove(id);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Returns all the classifications in the database
     *
     * @return set of classifications
     */
    public Set<Classification> getClassifications() {
        Set<Classification> classifications = new HashSet<>();
        try {
            classifications = classificationService.getClassifications();
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return classifications;
    }
    
    /**
     * Adds a new classification
     *
     * @param user
     *            - user object about to be added 
     *
     * @param restaurant
     *            - restaurant object about to be added 
     *
     * @param review
     *            - review object about to be added 
     *
     * @return true or false based on review addition
     */
    public void addReview(User user, Restaurant restaurant, Review review) {
        try {
            restaurantService.addReview(user, restaurant, review);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Adds a new classification
     *
     * @param classification
     *            - classification of the restaurant 
     *
     * @param restaurant
     *            - restaurant object to be updated 
     *
     * @return true or false based on review addition
     */
    public void assignClassification(Classification classification,
                                        Restaurant restaurant) {
        try {
            restaurantService.assignClassification(classification, restaurant);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
}
