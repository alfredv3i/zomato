package com.ideas2it.zomato.user.dao;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.address.Address;

/**
 * Interface implementation for User dao class
 */
public interface UserDao {

    /**
     * Inserts a new user into the database.
     *
     * @param user
     *            - user object about to be inserted
     *
     * @return true or false based on user group deletion
     */
    boolean insert(User user);
    
    /**
     * Updates the user in the database.
     *
     * @param user
     *            - user object to be updated
     */
    void update(User user);
    
    /**
     * Gets a user from database based on email.
     *
     * @param email
     *            - email of the user
     *
     * @return user specified by email
     */
    User getUserByEmail(String email); 
    
    /**
     * Gets a user from database based on id.
     *
     * @param id
     *            - id of the user
     *
     * @return user specified by id
     */
    User getUserById(int id);
    
    /**
     * Deletes the user from the database specified by id.
     *
     * @param id
     *            - id of user to be deleted
     */
    void delete(int id);
    
    /**
     * Returns the user if he has admin rights.
     *
     * @param id
     *            - id of the user
     *
     * @return admin user specified by id
     */
    User returnIfAdmin(int id);
}
