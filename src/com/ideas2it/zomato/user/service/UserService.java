package com.ideas2it.zomato.user.service;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.address.Address;

/**
 * Interface implementation for user service class
 */
public interface UserService {

    /**
     * Registers a new user.
     *
     * @param user
     *            - user object about to be added 
     *
     * @param address
     *            - address of the user to be added
     *
     * @return true or false based on user registration
     */
    boolean register(User user, Address address, int roleId);
    
    /**
     * Adds a new address to the user.
     *
     * @param user
     *            - user object about to be added 
     *
     * @param address
     *            - address of the user to be added
     *
     * @return true or false based on address assigning
     */
    boolean addAddress(User user, Address address);
    
    /**
     * Logs in the user to the application.
     *
     * @param email
     *            - email id of the user
     *
     * @param password
     *            - password of the user 
     */
    User login(String email, String password);
    
    /**
     * Logs out the user from the application.
     *
     * @param userId
     *            - id of the user
     *
     * @return true or false based on user logout
     */
    boolean logout(int id);
    
    /**
     * Resets the password for the user after verifying the security questions.
     *
     * @param email
     *            - email id of the user
     *
     * @param password
     *            - password of the user
     *
     * @param petName
     *            - pet name of the user
     *
     * @param bicycleName
     *            - bicycle name of the user 
     *
     * @return true or false based on password reset
     */
    boolean resetPassword(String email, String password, String petName, 
                     String bicycleName);
    
    /**
     * Gives admin access to the user.
     *
     * @param adminUser
     *            - adminUser who grants admin access 
     *
     * @param user
     *            - user who is assigned as admin
     *
     * @return true or false based on giving admin access
     */
    boolean giveAdminAccess(User adminUser, User user);
    
    /**
     * Returns the user specified by the id.
     *
     * @param id
     *            - id of the user
     *
     * @param user
     *            - user for the specified id 
     */
    User getUser(int id);
    
    /**
     * Returns the user specified by the id.
     *
     * @param id
     *            - id of the user
     */
    void updateUser(int id);
}
