package com.ideas2it.zomato.user.service.impl;

import java.lang.NullPointerException;
import java.util.HashSet;
import java.util.Set;

import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.common.Util;
import com.ideas2it.zomato.common.Constant;
import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.role.service.RoleService;
import com.ideas2it.zomato.role.service.impl.RoleServiceImpl;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.dao.UserDao;
import com.ideas2it.zomato.user.dao.impl.UserDaoImpl;
import com.ideas2it.zomato.user.User;

/**
 * Service class implementation for user object
 */
public class UserServiceImpl implements UserService {

    UserDao userDao = new UserDaoImpl();
    RoleService roleService = new RoleServiceImpl();

    /**
     * Registers a new user.
     *
     * @param user
     *            - user object about to be added 
     *
     * @param address
     *            - address of the user to be added
     *
     * @return true or false based on user registration
     */
    public boolean register(User user, Address address, int roleId) 
                   throws NullPointerException {
        boolean isSuccesful = false;
        if ((Util.validateEmail(user.getEmail())) ) { 
            address.setUser(user);
            Set <Address> addresses = new HashSet<>();
            addresses.add(address);
            user.setAddresses(addresses);            
            user.setJoinDate(Util.getDateTime());
            Role role = roleService.getRole(roleId);
            user.setRole(role);
            isSuccesful = userDao.insert(user);
        }
        return isSuccesful;
    }
    
    /**
     * Adds a new address to the user.
     *
     * @param user
     *            - user object about to be added 
     *
     * @param address
     *            - address of the user to be added
     *
     * @return true or false based on address assigning
     */
    public boolean addAddress(User user, Address address) 
                   throws NullPointerException {
        boolean isSuccesful = false;
        User expectedUser = userDao.getUserById(user.getId());
        if (expectedUser != null) {
            expectedUser.getAddresses().add(address);
            userDao.update(expectedUser);
            isSuccesful = true;
        }
        return isSuccesful;
    }
    
    /**
     * Logs in the user to the application.
     *
     * @param email
     *            - email id of the user
     *
     * @param password
     *            - password of the user 
     */
    public User login(String email, String password) 
                throws NullPointerException {
        User user = new User();
        if (Util.validateEmail(email)) {
            String currentDate = Util.getDateTime();
            user = userDao.getUserByEmail(email);
            if (!user.getPassword().equals(password)) {
                user = null;
            } else {
                user.setOnlineStatus(true);
                user.setLastLoginDate(currentDate);
                userDao.update(user);
            }
        }
        return user;
    }
    
    /**
     * Logs out the user from the application.
     *
     * @param userId
     *            - id of the user
     *
     * @return true or false based on user logout
     */
    public boolean logout(int id) throws NullPointerException {
        boolean success = false;
        User user = userDao.getUserById(id);
        if (null != user) {
            user.setOnlineStatus(false);
            userDao.update(user);
            success = true;
        }
        return success;
    }
    
    /**
     * Resets the password for the user after verifying the security questions.
     *
     * @param email
     *            - email id of the user
     *
     * @param password
     *            - password of the user
     *
     * @param petName
     *            - pet name of the user
     *
     * @param bicycleName
     *            - bicycle name of the user 
     *
     * @return true or false based on password reset
     */
    public boolean resetPassword(String email, String password, String petName, 
                               String bicycleName) throws NullPointerException {
        boolean isSuccesful = false;
        User user = userDao.getUserByEmail(email);
        if (null != user) {
            if (!(user.getPetName().equals(petName)) || 
                    !(user.getBicycleName().equals(bicycleName))) {
                isSuccesful = false;
            } else {
                user.setPassword(password);
                userDao.update(user);
                isSuccesful = true;
            }
        }
        return isSuccesful;
    }
    
    /**
     * Gives admin access to the user.
     *
     * @param adminUser
     *            - adminUser who grants admin access 
     *
     * @param user
     *            - user who is assigned as admin
     *
     * @return true or false based on giving admin access
     */
    public boolean giveAdminAccess(User adminUser, User user) 
                   throws NullPointerException {
        boolean isSuccesful = false;
        int adminRoleId = 1;
        RoleService roleService = new RoleServiceImpl();
        User expectedAdminUser = userDao.returnIfAdmin(adminUser.getId());
        if (null != expectedAdminUser) {
            Role role = roleService.getRole(adminRoleId);
            user.setRole(role);
            isSuccesful = true;
        }
        return isSuccesful;
    }
    
    /**
     * Returns the user specified by the id.
     *
     * @param id
     *            - id of the user
     *
     * @param user
     *            - user for the specified id 
     */
    public User getUser(int id) throws NullPointerException {
        return userDao.getUserById(id);
    }
    
    /**
     * Returns the user specified by the id.
     *
     * @param id
     *            - id of the user
     */
    public void updateUser(int id) {
        User user = userDao.getUserById(id);
        userDao.update(user);
    }
}
