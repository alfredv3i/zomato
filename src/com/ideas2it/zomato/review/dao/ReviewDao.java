package com.ideas2it.zomato.review.dao;

import com.ideas2it.zomato.review.Review;

/**
 * Interface implementation for Review dao class
 */
public interface ReviewDao {

   /**
     * Inserts a new review into the database.
     *
     * @param review
     *            - review object to be inserted
     */
    void insert(Review review);
    
    /**
     * Deletes the review from the database.
     *
     * @param id
     *            - id of review to be deleted
     */
    void delete(int id);
}
