package com.ideas2it.zomato.review.service;

import com.ideas2it.zomato.review.Review;

/**
 * Interface implementation for Review service class
 */
public interface ReviewService {

    /**
     * Adds a new review to the database
     *
     * @param review
     *            - review to be added
     */
    void add(Review review);
    
    /**
     * Deletes a review from the database
     *
     * @param review
     *            - review to be removed
     */
    void remove(Review review);
}
