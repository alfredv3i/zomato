package com.ideas2it.zomato.category.service.impl;

import java.util.Set;

import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.category.service.CategoryService;
import com.ideas2it.zomato.category.dao.CategoryDao;
import com.ideas2it.zomato.category.dao.impl.CategoryDaoImpl;
import com.ideas2it.zomato.restaurant.Restaurant;

/**
 * Service class implementation for category 
 */
public class CategoryServiceImpl implements CategoryService {

    CategoryDao categoryDao = new CategoryDaoImpl();

    /**
     * Adds a new category.
     *
     * @param category
     *            - category object about to be added 
     */
    public void add(Category category) {
        categoryDao.insert(category);
    }
    
    /**
     * Returns all the categories for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required 
     *
     * @return all food catgories belonging to the restaurant
     */
    public Set<Category> getCategories(int restaurantId) {
        return categoryDao.getCategories(restaurantId);
    }
    
    /**
     * Get all the categories from the database.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required 
     *
     * @return all food catgories belonging to the restaurant
     */
    public Set<Category> getAllCategories() {
        return categoryDao.getAllCategories();
    }
    
    /**
     * Removes the category from the database
     *
     * @param id
     *            - id of category
     */
    public void remove(int id) {
        categoryDao.delete(id);
    }
}
