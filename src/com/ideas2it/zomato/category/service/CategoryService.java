package com.ideas2it.zomato.category.service;

import java.util.Set;

import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.restaurant.Restaurant;

/**
 * Interface implementation for category service class
 */
public interface CategoryService {

    /**
     * Adds a new category.
     *
     * @param category
     *            - category object about to be added 
     */
    void add(Category category);
    
    /**
     * Returns all the categories for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required 
     *
     * @return all food catgories belonging to the restaurant
     */
    Set<Category> getCategories(int restaurantId);
    
    /**
     * Get all the categories from the database.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required 
     *
     * @return all food catgories belonging to the restaurant
     */
    Set<Category> getAllCategories();
    
    /**
     * Removes the category from the database
     *
     * @param id
     *            - id of category
     */
    void remove(int id);
}
