package com.ideas2it.zomato.menuItem.dao;

import java.util.Set;

import com.ideas2it.zomato.menuItem.MenuItem;

/**
 * Interface implementation for Menuitem dao class
 */
public interface MenuItemDao {

    /**
     * Inserts a new menuItem into the database.
     *
     * @param menuItem
     *            - menuItem object about to be inserted
     */
    void insert(MenuItem menuItem);
    
    /**
     * Updates a new menuItem into the database.
     *
     * @param menuItem
     *            - menuItem about to be updated
     */
    void update(MenuItem menuItem);
    
    /**
     * Returns the cart for the specified user.
     *
     * @param userId
     *            - id of the user
     *
     * @return cart object for the id
     */
    MenuItem getMenuItem(int id);
    
    /**
     * Deletes a menuItem from the database.
     *
     * @param id
     *            - id of menuitem to be deleted
     */
    void delete(int id);
    
    /**
     * Get the menu items for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose menu items are required 
     *
     * @param categoryId
     *            - category Id whose menu items are required 
     *
     * @return allmenuitems belonging to the restaurant
     */
    Set<MenuItem> getMenuItems(int restaurantId, int categoryId);
}
