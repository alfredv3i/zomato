package com.ideas2it.zomato.menuItem.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ideas2it.zomato.common.Config;
import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.menuItem.dao.MenuItemDao;

/**
 * Performs all the database related funcionalities for menuitem
 */
public class MenuItemDaoImpl implements MenuItemDao {

    private static final Logger logger = Logger.getLogger(MenuItemDaoImpl.class);

    /**
     * Inserts a new menuItem into the database.
     *
     * @param menuItem
     *            - menuItem object about to be inserted
     */
    public void insert(MenuItem menuItem) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.save(menuItem); 
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Updates a new menuItem into the database.
     *
     * @param menuItem
     *            - menuItem about to be updated
     */
    public void update(MenuItem menuItem) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.update(menuItem); 
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Returns the cart for the specified user.
     *
     * @param userId
     *            - id of the user
     *
     * @return cart object for the id
     */
    public MenuItem getMenuItem(int id) throws HibernateException {
        MenuItem menuItem = new MenuItem();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(MenuItem.class);
            criteria.add(Restrictions.eq("id", id));
            menuItem = (MenuItem)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
            session.close(); 
        }
        return menuItem;
    }
    
    /**
     * Deletes a menuItem from the database.
     *
     * @param id
     *            - id of menuitem to be deleted
     */
    public void delete(int id) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           MenuItem menuItem = (MenuItem) session.get(MenuItem.class, id);
           session.delete(menuItem);
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Get the menu items for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose menu items are required 
     *
     * @param categoryId
     *            - category Id whose menu items are required 
     *
     * @return allmenuitems belonging to the restaurant
     */
    public Set<MenuItem> getMenuItems(int restaurantId, int categoryId)
                         throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        Set<MenuItem> menuItems = new HashSet<>();
        try {
           transaction = session.beginTransaction();
           menuItems = new HashSet<>(session.createCriteria(MenuItem.class)
                           .createAlias("restaurant", "rest")
                           .createAlias("categories", "cat")
                           .add( Restrictions.eq("rest.id", restaurantId))
                           .add( Restrictions.eq("cat.id", categoryId))
                           .list());
           for (MenuItem menuItem : menuItems) {
               logger.debug(menuItem.getName());
           }
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
        return menuItems;
    }
}
