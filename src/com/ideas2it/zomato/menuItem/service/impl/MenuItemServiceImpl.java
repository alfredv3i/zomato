package com.ideas2it.zomato.menuItem.service.impl;

import java.util.Set;

import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.menuItem.service.MenuItemService;
import com.ideas2it.zomato.menuItem.dao.MenuItemDao;
import com.ideas2it.zomato.menuItem.dao.impl.MenuItemDaoImpl;

/**
 * Service class implementation for menuItem 
 */
public class MenuItemServiceImpl implements MenuItemService {

    MenuItemDao menuItemDao = new MenuItemDaoImpl();

    /**
     * Registers a new menu item to the restaurant
     *
     * @param menuItem
     *        - menuItem to be added
     *
     * @param foods
     *        - foods contained in the menu item
     *
     * @param categories
     *        - categories the menuitem belong to
     *
     * @param restaurant
     *        - restaurant of the menuitem
     */
    public void addMenuItem(MenuItem menuItem, Set<Food> foods, 
                  Set<Category> categories, Restaurant restaurant) {
        menuItem.setFoods(foods);
        menuItem.setCategories(categories);
        menuItem.setRestaurant(restaurant);
        menuItemDao.insert(menuItem);
    }
    
    /**
     * Get the menu items for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose menu items are required 
     *
     * @param categoryId
     *            - category Id whose menu items are required 
     *
     * @return allmenuitems belonging to the restaurant
     */
    public Set<MenuItem> getMenuItems(int restaurantId, int categoryId) {
        return menuItemDao.getMenuItems(restaurantId, categoryId);
    }
    
    /**
     * Updates a new menuItem
     *
     * @param menuItem
     *            - menuItem object about to be updated 
     */
    public void update(MenuItem menuItem) {
        menuItemDao.update(menuItem);
    }
    
    /**
     * Deletes a menuItem
     *
     * @param id
     *            - id of menuItem to be deleted
     */
    public void delete(int id) {
        menuItemDao.delete(id);
    }
    
    /**
     * Returns menu item specified by id
     *
     * @param id
     *            - id of menuItem to be returned
     */
    public MenuItem getMenuItem(int id) {
        return menuItemDao.getMenuItem(id);
    }
}
