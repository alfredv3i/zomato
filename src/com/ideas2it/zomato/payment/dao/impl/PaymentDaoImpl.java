package com.ideas2it.zomato.payment.dao.impl;

import java.util.List;
import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import com.ideas2it.zomato.common.Config;

import com.ideas2it.zomato.payment.Payment;
//import com.ideas2it.zomato.paymentOption.PaymentOption;
import com.ideas2it.zomato.payment.dao.PaymentDao;

/*
 * This class handles access between Payment services and hibernate 
 */
public class PaymentDaoImpl implements PaymentDao {
    
	/**
     * Updates the cart into the database.
     *
     * @param payment
     *            - payment to be updated
     */
    public void insert(Payment payment) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.saveOrUpdate(payment); 
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Returns the payment for the specified id.
     *
     * @param id
     *            - id of the payment
     *
     * @return payment object for the id
     */
    public Payment getPayment(int id) throws HibernateException {
        Payment payment = new Payment();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Payment.class);
            criteria.add(Restrictions.eq("id", id));
            payment = (Payment)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
            session.close(); 
        }
        return payment;
    }
}
