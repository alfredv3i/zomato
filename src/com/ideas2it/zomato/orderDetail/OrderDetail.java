package com.ideas2it.zomato.orderDetail;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.menuItem.MenuItem;

/**
 * This is model class for Cart Detail 
 */
public class OrderDetail {

    private int id;
    private Order order;
    private MenuItem menuItem;
    private int quantity;
    private float price;
    
    /*
     * Setters and Getters
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setOrder(Order order) {
        this.order = order;
    }
    
    public Order getOrder() {
        return order;
    }
    
    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }
    
    public MenuItem getMenuItem() {
        return menuItem;
    }
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public void setPrice(float price) {
        this.price = price;
    }
    
    public float getPrice() {
        return price;
    }
}
