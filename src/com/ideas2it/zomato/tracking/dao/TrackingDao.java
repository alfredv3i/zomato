package com.ideas2it.zomato.tracking.dao;

import com.ideas2it.zomato.tracking.Tracking;

/*
 * This interface handles access between Tracking services and hibernate 
 */
public interface TrackingDao {
    
    /**
     * Returns the tracking for the order
     *
     * @param orderId
     *            - order id for the required tracking 
     *
     * @return tracking of the order
     */
    Tracking getTracking(int orderId);
}
