package com.ideas2it.zomato.servlet;

import java.io.IOException;
import java.util.Set;
import java.util.HashSet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import com.ideas2it.zomato.review.Review;
import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.menuItem.controller.MenuItemController;
import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.common.Constant;

/**
 * Servlet implementation for user registration.
 */
public class MenuItemServlet extends HttpServlet {

    MenuItemController menuItemController = new MenuItemController();
    private static final Logger logger = Logger.getLogger(MenuItemServlet.class);

    private static final String REGISTER = "register";
    private static final String DISPLAY_CATEGORIES = "display categories";
    private static final String DISPLAY_ALL_CATEGORIES = "display all categories";
    private static final String DISPLAY_MENUITEMS = "display menu items";
    private static final String ADD_REVIEW = "add review";
    private static final String ADD_FOOD = "add food";
    private static final String REMOVE_FOOD = "remove food";
    private static final String DISPLAY_FOODS = "display foods";
    private static final String ADD_CATEGORY = "add category";
    private static final String REMOVE_CATEGORY = "remove category";

    /**
     * Inserts and updates tables into the database for user and usergroup
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
                       throws ServletException, IOException {
                       
        response.setContentType("text/html");
        String input = request.getParameter("submit");
        HttpSession session = request.getSession();
        
        switch (input) {
            case REGISTER :
                register(request, response);
                break;
            case ADD_REVIEW :
                addReview(request, response);
                break;
            case ADD_FOOD :
                addFood(request, response);
                break;
            case REMOVE_FOOD :
                removeFood(request, response);
                break;
            case ADD_CATEGORY :
                addCategory(request, response, session);
                break;
            case REMOVE_CATEGORY :
                removeCategory(request, response);
                break;
        }
    }
    
    /**
     * Displays menu item related information
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
                       throws ServletException, IOException {
                       
        response.setContentType("text/html");
        String input = request.getParameter("submit");
        HttpSession session = request.getSession();
        
        switch (input) {
            case DISPLAY_CATEGORIES :
                displayCategories(request, response, session);
                break;
            case DISPLAY_MENUITEMS :
                displayMenuItems(request, response, session);
                break;
            case DISPLAY_FOODS :
                displayFoods(request, response, session);
                break;
            case DISPLAY_ALL_CATEGORIES :
                displayAllCategories(request, response, session);
                break;
        }
    }
    
    /**
     * Adds a new review to the menu item.
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    public void addReview(HttpServletRequest request, HttpServletResponse response) 
                       throws ServletException, IOException {
        Review review = new Review(request.getParameter("description"));
        
        
    }
    
    /**
     * Adds a new food to the database
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    public void addFood(HttpServletRequest request, HttpServletResponse response) 
                       throws ServletException, IOException {
        Food food = new Food(request.getParameter("name"));
        menuItemController.addFood(food);
    }
    
    /**
     * Removes the food from the database
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    public void removeFood(HttpServletRequest request, HttpServletResponse response) 
                       throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        menuItemController.removeFood(id);
    }
    
    /**
     * Displays all the foods in database 
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    private void displayFoods(HttpServletRequest request, 
                              HttpServletResponse response, HttpSession session)
                              throws ServletException, IOException {
        Set<Food> foods = menuItemController.getFoods();
        request.setAttribute("foods", foods);
        String targetPage = request.getParameter("targetPage");
        request.getRequestDispatcher(targetPage).forward(request, response);
    }
    
    /**
     * Removes the category from the database
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    public void removeCategory(HttpServletRequest request, HttpServletResponse response) 
                       throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        menuItemController.removeCategory(id);
    }
    
    /**
     * Adds a new category to the database
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    public void addCategory(HttpServletRequest request, 
                            HttpServletResponse response, HttpSession session) 
                       throws ServletException, IOException {
        Category category = new Category(request.getParameter("name"));
        menuItemController.addCategory(category);
        displayAllCategories(request, response, session);
    }
    
    /**
     * Registers a new menu item to the restaurant
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    private void register(HttpServletRequest request, HttpServletResponse response)
                          throws ServletException, IOException {
        MenuItem menuItem = new MenuItem(request.getParameter("name"),
                               request.getParameter("description"),
                               Float.parseFloat(request.getParameter("price")));
        Set<Food> foods = (Set)request.getAttribute("foods");
        Set<Category> categories = (Set)request.getAttribute("categories");
        Restaurant restaurant = (Restaurant)request.getAttribute("restaurant");
        menuItemController.addMenuItem(menuItem, foods, categories, restaurant);
    }
    
    /**
     * Displays the food categories of the restaurant
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    private void displayCategories(HttpServletRequest request, 
                              HttpServletResponse response, HttpSession session)
                              throws ServletException, IOException {
        MenuItemController menuItemController = new MenuItemController();
        int restaurantId = Integer.parseInt(request.getParameter("id"));
        Set<Category> categories = menuItemController.getCategories(restaurantId);
        request.setAttribute("categories", categories);
        session.setAttribute("restaurantId", restaurantId);
        request.getRequestDispatcher(Constant.RESTAURANT)
                                     .forward(request, response);
    }
    
    /**
     * Displays all the food categories 
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    private void displayAllCategories(HttpServletRequest request, 
                              HttpServletResponse response, HttpSession session)
                              throws ServletException, IOException {
        MenuItemController menuItemController = new MenuItemController();
        Set<Category> categories = menuItemController.getAllCategories();
        request.setAttribute("categories", categories);
        String targetPage = request.getParameter("targetPage");
        request.getRequestDispatcher(targetPage).forward(request, response);
    }
    
    /**
     * Displays all the menu items for the restaurant category
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    private void displayMenuItems(HttpServletRequest request, 
                              HttpServletResponse response, HttpSession session)
                              throws ServletException, IOException {
        MenuItemController menuItemController = new MenuItemController();
        int categoryId = Integer.parseInt(request.getParameter("id"));
        int restaurantId = (Integer) session.getAttribute("restaurantId");
        Set<MenuItem> menuItems = menuItemController.getMenuItems(restaurantId,
                                                                  categoryId);
        session.setAttribute("menuItems", menuItems);
        request.getRequestDispatcher(Constant.MENUITEM_LIST)
                                     .forward(request, response);
    }
}
