package com.ideas2it.zomato.servlet;

import java.io.IOException;
import java.util.Set;
import java.util.HashSet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.tracking.Tracking;
import com.ideas2it.zomato.order.controller.OrderController;

/**
 * Servlet implementation for user registration.
 */
public class OrderServlet extends HttpServlet {

    OrderController orderController = new OrderController();
    
    private static final String PAY_NOW = "pay now";
    private static final String DISPLAY_ORDER = "display order";
    private static final String GET_TRACKING = "get tracking";

    /**
     * Inserts and updates tables into the database for user and usergroup
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
                       throws ServletException, IOException {
                       
        response.setContentType("text/html");
        String input = request.getParameter("submit");
        HttpSession session = request.getSession();
        
        switch (input) {
            case DISPLAY_ORDER :
                displayOrder(request, response, session);
                break;
            case PAY_NOW :
                createOrder(request, response, session);
                break;
            case GET_TRACKING :
                getTracking(request, response, session);
                break;
        }
    }
    
    /**
     * Creates a new order for the payment
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    private void createOrder(HttpServletRequest request, 
                        HttpServletResponse response, HttpSession session)
                        throws ServletException, IOException {
        int userId = (Integer)(session.getAttribute("userId"));
        int paymentId = (Integer)(session.getAttribute("paymentId"));
        orderController.createOrder(userId, paymentId);
    }
    
    /**
     * Displays the customer's order and details
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    private void displayOrder(HttpServletRequest request, 
                        HttpServletResponse response, HttpSession session)
                        throws ServletException, IOException {
        int userId = (Integer)(session.getAttribute("userId"));
        Order order = orderController.getOrder(userId);
        session.setAttribute("order", order);
    }
    
    /**
     * Displays the tracking for the order 
     *
     * @param request
     *        - request object of HTTPservlet
     *
     * @param response
     *        - response object of HTTPservlet
     */
    private void getTracking(HttpServletRequest request, 
                        HttpServletResponse response, HttpSession session)
                        throws ServletException, IOException {
        int orderId = (Integer)(session.getAttribute("orderId"));
        Tracking tracking = orderController.getTracking(orderId);
        session.setAttribute("tracking", tracking);
    }

}
