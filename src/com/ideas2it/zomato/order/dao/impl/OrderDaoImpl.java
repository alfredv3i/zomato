package com.ideas2it.zomato.order.dao.impl;

import org.hibernate.Query;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.order.dao.OrderDao;
import com.ideas2it.zomato.orderDetail.OrderDetail;
import com.ideas2it.zomato.common.Config;

/*
 * This class handles access between Food Order services and hibernate 
 */
public class OrderDaoImpl implements OrderDao {
    
    /**
     * Returns the order for the specified user.
     *
     * @param userId
     *            - id of the user
     *
     * @return order object for the id
     */
    public Order getOrder(int userId) throws HibernateException {
        Order order = new Order();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Order.class);
            criteria.add(Restrictions.eq("userId", userId));
            order = (Order)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
            session.close(); 
        }
        return order;
    }
    
    /**
     * Inserts the order into the database.
     *
     * @param order
     *            - order to be inserted
     */
    public void insert(Order order) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.saveOrUpdate(order); 
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
	
}
