package com.ideas2it.zomato.order.dao;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.orderDetail.OrderDetail;

/*
 * This class handles access between Order services and hibernate 
 */
public interface OrderDao {
    
    /**
     * Returns the order for the specified user.
     *
     * @param userId
     *            - id of the user
     *
     * @return order object for the id
     */
    Order getOrder(int userId);
    
    /**
     * Inserts the order into the database.
     *
     * @param order
     *            - order to be inserted
     */
    void insert(Order order);
}
