package com.ideas2it.zomato.order.service;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.orderDetail.OrderDetail;

/*
 * This interface handles access between Order DAO and controller layer 
 */
public interface OrderService {

    /**
     * Returns the order for the user
     *
     * @param userId
     *            - user id for the required order 
     *
     * @return Order of the user
     */
    Order getOrder(int userId);
    
    /**
     * Creates a new order for the payment
     *
     * @param userId
     *            - user id for the required order 
     *
     * @param paymentId
     *            - payment id for the required order 
     */
    void createOrder(int userId, int paymentId);

}
