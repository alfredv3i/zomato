package com.ideas2it.zomato.order.service.impl;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.order.service.OrderService;
import com.ideas2it.zomato.tracking.Tracking;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.service.impl.UserServiceImpl;
import com.ideas2it.zomato.orderDetail.OrderDetail;
import com.ideas2it.zomato.order.dao.OrderDao;
import com.ideas2it.zomato.order.dao.impl.OrderDaoImpl;
import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.payment.service.PaymentService;
import com.ideas2it.zomato.payment.service.impl.PaymentServiceImpl;
import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cart.service.CartService;
import com.ideas2it.zomato.cartDetail.CartDetail;
import com.ideas2it.zomato.cart.service.impl.CartServiceImpl;

/*
 * This class handles access between Order DAO and controller layer
 */
public class OrderServiceImpl implements OrderService {

    OrderDao orderDao = new OrderDaoImpl();
    UserService userService = new UserServiceImpl();
    CartService cartService = new CartServiceImpl();
    PaymentService paymentService = new PaymentServiceImpl();
    
    /**
     * Returns the order for the user
     *
     * @param userId
     *            - user id for the required order 
     *
     * @return Order of the user
     */
    public Order getOrder(int userId) {
        return orderDao.getOrder(userId);
    }
    
    /**
     * Creates a new order for the payment
     *
     * @param userId
     *            - user id for the required order 
     *
     * @param paymentId
     *            - payment id for the required order 
     */
    public void createOrder(int userId, int paymentId) {
        User user = userService.getUser(userId);
        Payment payment = paymentService.getPayment(paymentId);
        Order order = new Order(user, payment);
        Cart cart = cartService.getCart(userId);
        for (CartDetail cartDetail : cart.getCartDetails()) {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setPrice(cartDetail.getPrice());
            orderDetail.setMenuItem(cartDetail.getMenuItem());
            order.getOrderDetails().add(orderDetail);
        }
        Tracking tracking = new Tracking(order);
        order.setTracking(tracking);
        orderDao.insert(order);
    }
}
