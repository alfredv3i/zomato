package com.ideas2it.zomato.order;

import java.sql.Timestamp;

import java.util.Set;

import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.tracking.Tracking;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.orderDetail.OrderDetail;

/**
 * This is model class for Cart 
 */
public class Order {

    private int id;
    private Payment payment;
    private User user;
    private String time;
    private boolean dispatched;
    private boolean delivered;
    private Restaurant restaurant;
    private Tracking tracking;
    private Set<OrderDetail> orderDetails;
    
    public Order() {
    
    }
    
    public Order(User user, Payment payment) {
        this.user = user;
        this.payment = payment;
    }
    /*
     * Setters and Getters
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setPayment(Payment payment) {
        this.payment = payment;
    }
    
    public Payment getPayment() {
        return payment;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setTime(String time) {
        this.time = time;
    }
    
    public String getTime() {
        return time;
    }
    
    public void setDsipatched(boolean dispatched) {
        this.dispatched = dispatched;
    }
    
    public boolean getDispatched() {
        return dispatched;
    }
    
    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }
    
    public boolean getDelivered() {
        return delivered;
    }
    
    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
    
    public Restaurant getRestaurant() {
        return restaurant;
    }
    
    public void setTracking(Tracking tracking) {
        this.tracking = tracking;
    }
    
    public Tracking getTracking() {
        return tracking;
    }
    
    public void setOrderDetails(Set<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }
    
    public Set getOrderDetails() {
        return orderDetails;
    }
}
