package com.ideas2it.zomato.food.dao;

import java.util.Set;

import com.ideas2it.zomato.food.Food;

/**
 * Interface implementation for Food dao class
 */
public interface FoodDao {

    /**
     * Inserts a new food into the database.
     *
     * @param food
     *            - food object about to be inserted
     */
    void insert(Food food);
    
    /**
     * Deletes the food from the database specified by id.
     *
     * @param id
     *            - id of food to be deleted
     */
    void delete(int id);
    
    /**
     * Gets all the foods from the database.
     *
     * @return all foods in the database
     */
    Set<Food> getFoods(); 
}
