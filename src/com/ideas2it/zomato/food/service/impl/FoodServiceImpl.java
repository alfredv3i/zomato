package com.ideas2it.zomato.food.service.impl;

import java.util.Set;

import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.food.service.FoodService;
import com.ideas2it.zomato.food.dao.FoodDao;
import com.ideas2it.zomato.food.dao.impl.FoodDaoImpl;

/**
 * Service class implementation for food object
 */
public class FoodServiceImpl implements FoodService {

    FoodDao foodDao = new FoodDaoImpl();

    /**
     * Adds a new food to the database.
     *
     * @param food
     *            - food object about to be added 
     */
    public void add(Food food) {
        foodDao.insert(food);
    }
    
    /**
     * Removes the food specified by id.
     *
     * @param id
     *            - id of food 
     */
    public void remove(int id) {
        foodDao.delete(id);
    }
    
    /**
     * Gets all the foods from the database.
     *
     * @return all foods in the database
     */
    public Set<Food> getFoods() {
         return foodDao.getFoods();
    }
}
